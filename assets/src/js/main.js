//load all required scripts
requirejs(
	[
		//include jQuery from here - no extra HTTP request
		'jquery',
	
		'scripts/anchors.external.popup',
		'scripts/standard.accordion',
		'scripts/custom.select',
		'scripts/magnific.popup',
		'scripts/responsive.video',
		
		'scripts/tabs',
		'scripts/aspect.ratio',
		'scripts/lazy.images',
		
		//toggles a class of list-view at the width specified by collapse-at-###
		'scripts/blocks',

		//search must come before nav because of escape key propagation/binding
		'scripts/search',
		'scripts/nav',
		'scripts/hero',
		'scripts/conditions'
		
	],
	function() {
	
		var $document = $(document);
			
		$document
			.on('tabChanged',function(e,el) {
				$document.trigger('updateTemplate');
			});
			
			
		
		
});