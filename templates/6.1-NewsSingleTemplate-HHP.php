<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
						
							<div class="sec-nav">
							
								<div class="sec-nav-links">
									<button class="nav-button t-fa-abs fa-navicon">Menu</button>
									<ul>
										<li><a href="#" class="selected">News Item Title</a></li>
									</ul>
								</div><!-- .sec-nav-links -->
							
								<div class="breadcrumbs">
									<a href="#">The Latest</a>
									<a href="#">News Item Title</a>
								</div>
								
							</div><!-- .sec-nav -->
								
							<div class="article-head">
								<div class="hgroup">
									<h2>News Item Title</h2>
									<span class="subtitle">Sub Title</span>
								</div>
								
								<div class="article-head-meta">
									<span class="t-fa fa-calendar">October 20, 2014</span>
								</div>
								
							</div><!-- .article-head -->
							
							<div class="main-body">
								<div class="content article-body">
							
									<div class="grid fill pad40">
										<div class="col col-2 sm-col-1">
											<div class="item">
											
												<p>
													Vivamus elementum elementum lacus. Mauris auctor tempus justo, ac tincidunt turpis tempor pharetra. 
													Aenean facilisis diam mi, vitae pellentesque arcu vestibulum id. Fusce fermentum, diam ut dapibus posuere, 
													purus tortor vestibulum tortor, vel rhoncus lacus metus nec nisl. Maecenas congue imperdiet pretium. 
													Sed interdum tempus sem, ut varius arcu aliquam a.
												</p>

												<p>
													Maecenas venenatis, elit eget posuere luctus, nunc sem malesuada purus, vel consectetur quam purus quis est. 
													Nulla eget ipsum porta, consequat augue id, porttitor augue. Suspendisse euismod, diam at lacinia feugiat, 
													mauris libero lacinia felis, eu tincidunt libero nulla convallis est. Nunc nec turpis a libero accumsan fringilla. 
													Donec porta mi in massa ultrices venenatis. Ut non eleifend sem. Integer nibh mauris, viverra ac ante nec, 
													molestie bibendum tellus. Donec egestas quis lorem sit amet volutpat.
												</p>

												<p>
													Fusce magna risus, elementum ut commodo tempus, egestas at nisi. Cras consequat cursus erat ac tempus. 
													Morbi tempor sit amet sapien ac posuere. Etiam at leo eleifend, malesuada enim a, volutpat eros. Nunc fermentum 
													condimentum ultricies. Aliquam nibh arcu, suscipit a tempus nec, facilisis non metus. Nam id facilisis dolor. 
													Duis in massa rhoncus, accumsan mauris id, vulputate nunc. Donec sagittis leo lorem, ut cursus felis convallis non.
												</p>

												<p>
													Etiam dignissim ex quis lectus sollicitudin bibendum. In ultrices ultrices arcu, vitae auctor neque rhoncus sit amet. 
													Vivamus lacinia urna mauris, eget volutpat erat tempus vel. Brazil, Japan, Argentina, France, Holland and Singapore.
												</p>
												
												<ul>
													<li>Etiam dignissim ex quis lectus sollicitudin bibendum.</li>
													<li>Etiam dignissim ex quis lectus sollicitudin bibendum.</li>
													<li>Etiam dignissim ex quis lectus sollicitudin bibendum.</li>
												</ul>
												
												<ol>
													<li>Etiam dignissim ex quis lectus sollicitudin bibendum.</li>
													<li>Etiam dignissim ex quis lectus sollicitudin bibendum.</li>
													<li>Etiam dignissim ex quis lectus sollicitudin bibendum.</li>
												</ol>
												
													<a href="#button" class="button">Button</a>  
													
													<br />
													
													<a href="#button" class="button white">White Button</a>
												
												<p>
													<a href="#lnk" class="link">Inline Link</a>
													<a href="#hover" class="hover">Link Hovered</a>
													<a href="#visited" class="visited">Link Visited</a>
												</p>
												
												<h1>Header - H1</h1>
												<h2>Header - H1</h2>
												<h3>Header - H1</h3>
												<h4>Header - H1</h4>
												<h5>Header - H1</h5>
												<h6>Header - H1</h6>
												
											</div><!-- .item -->
										</div><!-- .col -->
									</div><!-- .grid -->

								</div><!-- .content -->
								<aside class="sidebar">
									
									<div class="archives-mod s-mod dark-bg blue-bg">
										<h4 class="s-mod-title">Archives</h4>
										
										<div class="acc with-indicators">
											<div class="acc-item">
												<div class="acc-item-handle">
													2014 (5)
												</div><!-- .acc-item-handle -->
												<div class="acc-item-content">
													<ul>
														<li class="selected"><a href="#">October (3)</a></li>
														<li><a href="#">September (2)</a></li>
													</ul>
												</div><!-- .acc-item-content -->
											</div><!-- .acc-item -->
											<div class="acc-item">
												<div class="acc-item-handle">
													2013 (10)
												</div><!-- .acc-item-handle -->
												<div class="acc-item-content">
													<ul>
														<li><a href="#">October (3)</a></li>
														<li><a href="#">September (2)</a></li>
														<li><a href="#">September (2)</a></li>
														<li><a href="#">September (2)</a></li>
													</ul>
												</div><!-- .acc-item-content -->
											</div><!-- .acc-item -->
											<div class="acc-item">
												<div class="acc-item-handle">
													2012 (15)
												</div><!-- .acc-item-handle -->
												<div class="acc-item-content">
													<ul>
														<li><a href="#">October (3)</a></li>
														<li><a href="#">October (3)</a></li>
														<li><a href="#">October (3)</a></li>
														<li><a href="#">October (3)</a></li>
														<li><a href="#">September (2)</a></li>
													</ul>
												</div><!-- .acc-item-content -->
											</div><!-- .acc-item -->
										</div><!-- .acc -->
										
									</div><!-- .archives-mod -->
									
								</aside><!-- .sidebar -->
							</div><!-- .main-body -->
						
						</div><!-- .item-content -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="nopad light-green-bg">
		<div class="sw">
			<?php include('inc/i-book-contact.php'); ?>
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>