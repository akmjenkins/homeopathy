			<div class="book-appointment-section">
				<div class="hgroup">
					<h3>Book An Appointment</h3>
					<span class="subtitle">Schedule an appointment by filling out the form or call us at 506 855 2230.</span>
				</div><!-- .hgroup -->
				
				<form action="/">
					<input type="text" name="name" placeholder="Name">
					<input type="email" name="email" placeholder="Email">
					<input type="tel" pattern="\d+" name="tel" placeholder="Phone">
					<button class="big button" type="submit">Submit</button>
				</form><!-- .book-appointment-form -->
			</div><!-- .book-appointment-section -->