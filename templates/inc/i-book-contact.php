<div class="grid nopad eqh vcenter ov-blocks">
	<div class="col col-2 sm-col-1">
		<div class="item ov-block">
			
			<div class="ov-block-content">
			
				<div class="hgroup">
					<h3>Contact Us</h3>
					<span class="subtitle">Cras convallis orci a erat aliquet luctus</span>
				</div><!-- .hgroup -->
				
				<div class="btn-group">
					<span class="fa-button t-fa-abs fa-phone">Phone</span>
					<span class="fa-button t-fa-abs fa-envelope-o">E-mail</span>
				</div><!-- .btn-group -->
				
			</div><!-- .ov-block-content -->
			
		</div><!-- .item -->
	</div><!-- .col -->
	<div class="col col-2 sm-col-1">
		<div class="item ov-block">
			
			<div class="ov-block-content">
			
				<div class="hgroup">
					<h3>Book Online Consultation</h3>
					<span class="subtitle">Cras convallis orci a erat aliquet luctus</span>
				</div><!-- .hgroup -->
				
				<div class="btn-group">
					<a href="#" class="big button">Log In</a>
					<a href="#" class="big button">Sign Up</a>
				</div><!-- .btn-group -->

			</div><!-- .ov-block-content -->
			
		</div><!-- .item -->
	</div><!-- .col -->
</div><!-- .grid -->