<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
						
							<div class="sec-nav">
							
								<div class="sec-nav-links">
									<button class="nav-button t-fa-abs fa-navicon">Menu</button>
									<ul>
										<li><a href="#" class="selected">Contact</a></li>
									</ul>
								</div><!-- .sec-nav-links -->
							
								<div class="breadcrumbs">
									<a href="#">Contact</a>
								</div>
								
							</div><!-- .sec-nav -->
							
							<div class="article-head">
								<div class="hgroup">
									<h2>Contact</h2>
									<span class="subtitle">Tellus sed arcu ultrices ornare in.</span>
								</div>
							</div><!-- .article-head -->
						
							<div class="main-body">
								<div class="content article-body">						
									<div class="grid fill pad40">
										<div class="col col-2 sm-col-1">
											<div class="item">
											
												<p>
													Sed quam nunc, posuere sed ante vitae, semper imperdiet sem. Cras vulputate id metus eget luctus. Nullam condimentum .
												</p>

												<h4>Locations</h4>
												
												<div class="has-lightslider location-slider">
												
													<div class="btn-group ls-delegate-links">
														<span class="button">Location One</span>
														<span class="button">Location Two</span>
														<span class="button">Location Three</span>
													</div><!-- .btn-group -->
												
													<ul class="lightslider" data-mode="slide">
													
														<li>
														
															<div class="grid pad40">
																<div class="col col-2 xs-col-1">
																	<div class="item">
																	
																		<address>
																			15 Penny Lane, West Main St.<br />
																			Moncton NB, E1E 4W4
																		</address>
																		
																		<br />
																		
																		<div class="rows">
																			<span class="row">
																				<span class="l">Phone:</span>
																				<span class="r">506 855 2230</span>
																			</span>
																			
																			<span class="row">
																				<span class="l">Fax:</span>
																				<span class="r">506 855 2230</span>
																			</span>
																		</div><!-- .rows -->
																		
																	</div><!-- .item -->
																</div><!-- .col -->
																<div class="col col-2 xs-col-1">
																	<div class="item">
																		<h6>Office Hours</h6>
																		
																		Monday 8:00am - 7:30pm <br />
																		Tuesday 8:00am - 5:00pm <br />
																		Wednesday 8:00am - 5:00pm <br />
																		Thursday 8:00am - 5:00pm <br />
																		Friday 8:00am - 1:00pm
																	</div><!-- .item -->
																</div><!-- .col -->
															</div><!-- .grid -->
															
														</li>
														
														<li>
														
															<div class="grid pad40">
																<div class="col col-2 xs-col-1">
																	<div class="item">
																	
																		<address>
																			15 Penny Lane, West Main St.<br />
																			Moncton NB, E1E 4W4
																		</address>
																		
																		<br />
																		
																		<div class="rows">
																			<span class="row">
																				<span class="l">Phone:</span>
																				<span class="r">506 855 2230</span>
																			</span>
																			
																			<span class="row">
																				<span class="l">Fax:</span>
																				<span class="r">506 855 2230</span>
																			</span>
																		</div><!-- .rows -->
																		
																	</div><!-- .item -->
																</div><!-- .col -->
																<div class="col col-2 xs-col-1">
																	<div class="item">
																		<h6>Office Hours</h6>
																		
																		Monday 8:00am - 7:30pm <br />
																		Tuesday 8:00am - 5:00pm <br />
																		Wednesday 8:00am - 5:00pm <br />
																		Thursday 8:00am - 5:00pm <br />
																		Friday 8:00am - 1:00pm
																	</div><!-- .item -->
																</div><!-- .col -->
															</div><!-- .grid -->
														
														</li>
														
														<li>
														
															<div class="grid pad40">
																<div class="col col-2 xs-col-1">
																	<div class="item">
																	
																		<address>
																			15 Penny Lane, West Main St.<br />
																			Moncton NB, E1E 4W4
																		</address>
																		
																		<br />
																		
																		<div class="rows">
																			<span class="row">
																				<span class="l">Phone:</span>
																				<span class="r">506 855 2230</span>
																			</span>
																			
																			<span class="row">
																				<span class="l">Fax:</span>
																				<span class="r">506 855 2230</span>
																			</span>
																		</div><!-- .rows -->
																		
																	</div><!-- .item -->
																</div><!-- .col -->
																<div class="col col-2 xs-col-1">
																	<div class="item">
																		<h6>Office Hours</h6>
																		
																		Monday 8:00am - 7:30pm <br />
																		Tuesday 8:00am - 5:00pm <br />
																		Wednesday 8:00am - 5:00pm <br />
																		Thursday 8:00am - 5:00pm <br />
																		Friday 8:00am - 1:00pm
																	</div><!-- .item -->
																</div><!-- .col -->
															</div><!-- .grid -->
														
														</li>
														
													</ul><!-- .lightslider -->
												</div><!-- .location-slider -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										<div class="col col-2 sm-col-1">
											<div class="item">
											
												<form action="/" method="post" class="body-form dark-fields">
													<fieldset>
										
														<span class="field-wrap t-fa-abs fa-user">
															<input type="text" name="name" placeholder="Name">
														</span><!-- .field-wrap -->
														
														<span class="field-wrap t-fa-abs fa-lock">
															<input type="email" name="email" placeholder="E-mail Address">
														</span><!-- .field-wrap -->
														
														<span class="field-wrap t-fa-abs fa-pencil ta-wrap">
															<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>
														</span>
														
														<button type="submit" class="button big">Submit</button>
														
													
													</fieldset>
												</form><!-- .body-form -->
											
											</div><!-- .item -->
										</div><!-- .col -->
									</div><!-- .grid -->
								</div><!-- .content -->
							</div><!-- .main-body -->
						
						</div><!-- .item-content -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>