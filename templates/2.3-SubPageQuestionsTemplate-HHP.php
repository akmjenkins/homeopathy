<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
									
							<div class="article-head">
								<div class="hgroup">
									<h2>Ask A Question</h2>
									<span class="subtitle">Aliquam at dui nec tortor cursus semper.</span>
								</div>
							</div><!-- .article-head -->
		
						</div><!-- .item-content -->
					
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg blue-bg">
		<div class="sw">
		
			<div class="ask-a-question">
			
				<p>Vivamus ultricies malesuada ante rhoncus sollicitudin. Donec tincidunt iaculis aliquam. Sed ornare, massa quis iaculis ullamcorper, diam arcu auctor est, in ornare enim</p>
				
				<form action="" method="get" class="body-form">
					<fieldset>
						<input type="text" name="question" placeholder="Type your question here...">
						<button class="button big" type="submit">Submit</button>
					</fieldset>
				</form><!-- .body-form -->
				
			</div><!-- .ask-a-question -->
		
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="article-head">
				<div class="hgroup">
					<h2>Previously Asked Questions</h2>
				</div>
			</div><!-- .article-head -->
		
			<div class="grid eqh fill vcenter round-blocks">
			
				<div class="col col-3 sm-col-2 xs-col-1">
					<a class="item dark-bg blue-bg abs-button" href="#">
					
						<div class="pad-20 center">
							<h4>What are the three levels of homeopathic therapy?</h4>
							
							<span class="btn-wrap">
								<span class="button big uc">Read More</span>
							</span><!-- .btn-wrap -->
						</div><!-- .pad-20 -->
						
					</a><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3 sm-col-2 xs-col-1">
					<a class="item dark-bg blue-bg abs-button" href="#">
					
						<div class="pad-20 center">
							<h4>What is the relationship between symptoms and homeopathy?</h4>
							
							<span class="btn-wrap">
								<span class="button big uc">Read More</span>
							</span><!-- .btn-wrap -->
						</div><!-- .pad-20 -->
						
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-2 xs-col-1">
					<a class="item dark-bg blue-bg abs-button" href="#">
					
						<div class="pad-20 center">
							<h4>Can Such Small Doses Be Effective?</h4>
							
							<span class="btn-wrap">
								<span class="button big uc">Read More</span>
							</span><!-- .btn-wrap -->
						</div><!-- .pad-20 -->
						
					</a><!-- .item -->
				</div><!-- .col -->
				
			
				<div class="col col-3 sm-col-2 xs-col-1">
					<a class="item dark-bg blue-bg abs-button" href="#">
					
						<div class="pad-20 center">
							<h4>What are the three levels of homeopathic therapy?</h4>
							
							<span class="btn-wrap">
								<span class="button big uc">Read More</span>
							</span><!-- .btn-wrap -->
						</div><!-- .pad-20 -->
						
					</a><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3 sm-col-2 xs-col-1">
					<a class="item dark-bg blue-bg abs-button" href="#">
					
						<div class="pad-20 center">
							<h4>What is the relationship between symptoms and homeopathy?</h4>
							
							<span class="btn-wrap">
								<span class="button big uc">Read More</span>
							</span><!-- .btn-wrap -->
						</div><!-- .pad-20 -->
						
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-2 xs-col-1">
					<a class="item dark-bg blue-bg abs-button" href="#">
					
						<div class="pad-20 center">
							<h4>Can Such Small Doses Be Effective?</h4>
							
							<span class="btn-wrap">
								<span class="button big uc">Read More</span>
							</span><!-- .btn-wrap -->
						</div><!-- .pad-20 -->
						
					</a><!-- .item -->
				</div><!-- .col -->

				
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="nopad light-green-bg">
		<div class="sw">
			<?php include('inc/i-book-contact.php'); ?>
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>