<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
						
							<div class="sec-nav">
							
								<div class="sec-nav-links">
									<button class="nav-button t-fa-abs fa-navicon">Menu</button>
									<ul>
										<li><a href="#">About Dr. Ravi</a></li>
										<li><a href="#" class="selected">Credentials</a></li>
										<li><a href="#">Regulations</a></li>
										<li><a href="#">Ask A Question</a></li>
										<li><a href="#">Success Stories</a></li>
									</ul>
								</div><!-- .sec-nav-links -->
							
								<div class="breadcrumbs">
									<a href="#">Dr. Ravi</a>
									<a href="#">Credentials</a>
								</div>
								
							</div><!-- .sec-nav -->
							
							<div class="article-head">
								<div class="hgroup">
									<h2>Regulations</h2>
									<span class="subtitle">Tellus sed arcu ultrices ornare in.</span>
								</div>
							</div><!-- .article-head -->
								
							<div class="main-body">
								<div class="content article-body">
								
									<p>
										Aenean ut sapien quis est ultricies dignissim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
										Mauris in orci vitae erat consectetur eleifend imperdiet eget nunc. Proin sit amet tempus lacus. Curabitur a volutpat augue. 
										Donec eu nisi ut nisl blandit feugiat in a eros. Praesent ac purus id ligula finibus luctus. Morbi hendrerit semper neque, ut finibus 
										mauris suscipit non. Duis dignissim feugiat est, ac egestas ligula posuere interdum. Suspendisse blandit aliquam semper. Nulla 
										condimentum sapien non urna interdum, vel malesuada neque ultricies. Donec a tellus sed arcu ultrices ornare in nec est. Nunc 
										dapibus molestie justo at vulputate. Morbi non leo venenatis, placerat sem vel, viverra metus. Vestibulum blandit nunc sit amet odio 
										venenatis facilisis. Duis orci ex, sodales quis condimentum eget, rutrum in massa.Brazil, Japan, Argentina, France, Holland and Singapore.
									</p>
									
									<div class="dark-bg blue-bg divider-header">
										<h5>Regulated Health Professions Act, 1991</h5>
									</div>
									
									<p>
										Aenean ut sapien quis est ultricies dignissim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
										Mauris in orci vitae erat consectetur eleifend imperdiet eget nunc. Proin sit amet tempus lacus. Curabitur a volutpat augue. 
										Donec eu nisi ut nisl blandit feugiat in a eros. Praesent ac purus id ligula finibus luctus. Morbi hendrerit semper neque, ut finibus mauris 
										suscipit non. Duis dignissim feugiat est, ac egestas ligula posuere interdum. Suspendisse blandit aliquam semper. Nulla condimentum sapien 
										non urna interdum, vel malesuada neque ultricies. Donec a tellus sed arcu ultrices ornare in nec est. Nunc dapibus molestie justo at vulputate. 
										Morbi non leo venenatis, placerat sem vel, viverra metus. Vestibulum blandit nunc sit amet odio venenatis facilisis. Duis orci ex, sodales 
										quis condimentum eget, rutrum in massa.
									</p>
									
									<div class="dark-bg blue-bg divider-header">
										<h5>Regulated Health Professions Act, 1991</h5>
									</div>
									
									<p>
										Aenean ut sapien quis est ultricies dignissim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
										Mauris in orci vitae erat consectetur eleifend imperdiet eget nunc. Proin sit amet tempus lacus. Curabitur a volutpat augue. 
										Donec eu nisi ut nisl blandit feugiat in a eros. Praesent ac purus id ligula finibus luctus. Morbi hendrerit semper neque, ut finibus mauris 
										suscipit non. Duis dignissim feugiat est, ac egestas ligula posuere interdum. Suspendisse blandit aliquam semper. Nulla condimentum sapien 
										non urna interdum, vel malesuada neque ultricies. Donec a tellus sed arcu ultrices ornare in nec est. Nunc dapibus molestie justo at vulputate. 
										Morbi non leo venenatis, placerat sem vel, viverra metus. Vestibulum blandit nunc sit amet odio venenatis facilisis. Duis orci ex, sodales 
										quis condimentum eget, rutrum in massa.
									</p>
									
								</div><!-- .content -->
							</div><!-- .main-body -->
						
						</div><!-- .item-content -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>

	<section class="nopad light-green-bg">
		<div class="sw">
			<?php include('inc/i-book-contact.php'); ?>
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>