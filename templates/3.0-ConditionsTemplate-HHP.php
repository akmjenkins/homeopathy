<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="nopad">
		<div class="sw">
		
			<div class="grid eqh fill nopad">
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<div class="item-content">
									
							<div class="article-head">
								<div class="hgroup nosep">
									<h2>Ask A Question</h2>
									<span class="subtitle">Aliquam at dui nec tortor cursus semper.</span>
								</div>
							</div><!-- .article-head -->
		
						</div><!-- .item-content -->
					
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg blue-bg">
		<div class="sw">
		
			<div class="ask-a-question">
			
				<p>Vivamus ultricies malesuada ante rhoncus sollicitudin. Donec tincidunt iaculis aliquam. Sed ornare, massa quis iaculis ullamcorper, diam arcu auctor est, in ornare enim</p>
				
				<form action="" method="get" class="body-form">
					<fieldset>
						<input type="text" name="question" placeholder="Type your question here...">
						<button class="button big" type="submit">Submit</button>
					</fieldset>
				</form><!-- .body-form -->
				
			</div><!-- .ask-a-question -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg green-bg">
		<div class="sw">
			
			<div class="section-header">
				<h3>Conditions Treated</h3>
				<span class="subtitle">Nam bibendum elit ex, a tristique odio auctor non. Nullam pellentesque leo est, venenatis mattis lacus blandit vel. Nam pretium, sapien ut </span>
			</div><!-- .section-header -->
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
	<section class="blue-bg nopad">
			<div class="sw conditions-wrap">
			
				<div class="conditions-selector selector with-arrow">
					<select name="conditions-selector">
						<option value="">-- Select a Condition --</option>
						<option value="Circulatory">Circulatory</option>
						<option value="Immune">Immune</option>
						<option value="Muscular">Muscular</option>
						<option value="Nervous">Nervous</option>
						<option value="Reproductive">Reproductive</option>
						<option value="Respiratory">Respiratory</option>
						<option value="Skeletal">Skeletal</option>
						<option value="Urinary">Urinary</option>
					</select>
					<span class="value">&nbsp;</span>
				</div><!-- .conditions-selector -->
			
				<div class="grid eqh conditions-grid dark-bg blue-bg">
				
					<div class="col col-4">
						<div class="item bounce" data-title="Circulatory" data-src="./inc/i-circulatory-conditions.php">
							
							<div class="pad-20">
								<span class="ico-wrap"><span class="c-f-abs c-f-circulatory">Circulatory</span></span>
								<span class="title">Circulatory</span>
							</div><!-- .pad-20 -->
							
						</div>
					</div><!-- .col -->
					
					<div class="col col-4">
						<div class="item bounce" data-title="Immune" data-src="./inc/i-immune-conditions.php">
							
							<div class="pad-20">
								<span class="ico-wrap"><span class="c-f-abs c-f-immune">Immune</span></span>
								<span class="title">Immune</span>
							</div><!-- .pad-20 -->
							
						</div>
					</div><!-- .col -->
					
					<div class="col col-4">
						<div class="item bounce" data-title="Muscular" data-src="./inc/i-muscular-conditions.php">
							
							<div class="pad-20">
								<span class="ico-wrap"><span class="c-f-abs c-f-muscular">Muscular</span></span>
								<span class="title">Muscular</span>
							</div><!-- .pad-20 -->
							
						</div>
					</div><!-- .col -->
					
					<div class="col col-4">
						<div class="item bounce" data-title="Nervous" data-src="./inc/i-nervous-conditions.php">
							
							<div class="pad-20">
								<span class="ico-wrap"><span class="c-f-abs c-f-nervous">Nervous</span></span>
								<span class="title">Nervous</span>
							</div><!-- .pad-20 -->
							
						</div>
					</div><!-- .col -->
					
					<div class="col col-4">
						<div class="item bounce" data-title="Reproductive" data-src="./inc/i-reproductive-conditions.php">
							
							<div class="pad-20">
								<span class="ico-wrap"><span class="c-f-abs c-f-reproductive">Reproductive</span></span>
								<span class="title">Reproductive</span>
							</div><!-- .pad-20 -->
							
						</div>
					</div><!-- .col -->
					
					<div class="col col-4">
						<div class="item bounce" data-title="Respiratory" data-src="./inc/i-respiratory-conditions.php">
							
							<div class="pad-20">
								<span class="ico-wrap"><span class="c-f-abs c-f-respiratory">Respiratory</span></span>
								<span class="title">Respiratory</span>
							</div><!-- .pad-20 -->
							
						</div>
					</div><!-- .col -->
					
					<div class="col col-4">
						<div class="item bounce" data-title="Skeletal" data-src="./inc/i-skeletal-conditions.php">
							
							<div class="pad-20">
								<span class="ico-wrap"><span class="c-f-abs c-f-skeletal">Skeletal</span></span>
								<span class="title">Skeletal</span>
							</div><!-- .pad-20 -->
							
						</div>
					</div><!-- .col -->
					
					<div class="col col-4">
						<div class="item bounce" data-title="Urinary" data-src="./inc/i-urinary-conditions.php">
							
							<div class="pad-20">
								<span class="ico-wrap"><span class="c-f-abs c-f-urinary">Urinary</span></span>
								<span class="title">Urinary</span>
							</div><!-- .pad-20 -->
							
						</div>
					</div><!-- .col -->
					
				</div><!-- .grid -->
				
				<div class="condition-selected">
					<span class="t-fa-abs fa-spin fa-refresh">Loading...</span>
					<div class="condition-content">
					</div>
				</div><!-- .condition-selected -->
			
			</div><!-- .sw -->
		</div><!-- .conditions-section -->
	</section>
	
	<section class="nopad light-green-bg">
		<div class="sw">
			<?php include('inc/i-book-contact.php'); ?>
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>